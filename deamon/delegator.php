<?php
require_once '../../../vendor/autoload.php';
require_once '../../../config/novum.cbs/propel/config.php';
require_once '../../../config/novum.cbs/config.php';

use ApiNovumCbs\Console;
use ApiNovumCbs\CounterHelper;
use ApiNovumCbs\Sender;
use ApiNovumCbs\TeleportMachine;
use Model\Custom\NovumCbs\Data\InitialPeopleQuery;

$oTeleportMachine = new TeleportMachine();

if($oTeleportMachine->isTheBeginningOfTime())
{
    $oInitialPeopleQuery = new InitialPeopleQuery();

    $oInitialPeople = $oInitialPeopleQuery->findOneByJaartal($oTeleportMachine->getCurrentYear());


    $aPeopleSets = [
        ['getter' => 'getLeeftijd020', 'min' => 0,  'max' => 20, 'tag' => 'age_020'],
        ['getter' => 'getLeeftijd2045', 'min' => 20,  'max' => 45, 'tag' => 'age_2045'],
        ['getter' => 'getLeeftijd4565', 'min' => 45,  'max' => 65, 'tag' => 'age_4565'],
        ['getter' => 'getLeeftijd6580', 'min' => 65,  'max' => 80, 'tag' => 'age_6580']
    ];

    foreach($aPeopleSets as $aPeopleSet)
    {
        Console::log("Start new set " . $aPeopleSet['tag']);

        $oCounterHelper = new CounterHelper($oInitialPeople->{$aPeopleSet['getter']}(), $aPeopleSet['tag']);
        while($oCounterHelper->needMore())
        {
            $iCurrentTime = $oTeleportMachine->getCurrentTime()->getTimestamp();


            Sender::send('cbs', 'create-person', ['age' => rand($aPeopleSet['min'], $aPeopleSet['max']), 'current_time' => $iCurrentTime]);


            $oCounterHelper->increment();
        }
    }


    $oCounterHelper = new CounterHelper($oInitialPeople->getBurgerlijkeGehuwd(), 'weddings');
    while($oCounterHelper->needMore())
    {
        Sender::send('cbs', 'make-wedding', ['age' => rand(90, 106)]);
        $oCounterHelper->increment();
    }


    $oCounterHelper = new CounterHelper($oInitialPeople->getBurgerlijkeGescheiden(), 'divorces');
    while($oCounterHelper->needMore())
    {
        Sender::send('cbs', 'make-divorce', ['age' => rand(90, 106)]);
        $oCounterHelper->increment();
    }


}
