<?php

return [
    'api_version' => 2,
    'config_dir'  => 'novum.cbs',
    'namespace'   => 'ApiNovumCbs',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'api.cbs.demo.novum.nu',
    'dev_domain'  => 'api.cbs.innovatieapp.nl',
    'test_domain' => 'api.cbs.test.demo.novum.nu',
];

