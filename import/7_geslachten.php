<?php
require_once '../../../vendor/autoload.php';
require_once '../../../config/novum.cbs/propel/config.php';
require_once '../../../config/novum.cbs/config.php';

use Model\Custom\NovumCbs\Stam\Geslacht;
use Model\Custom\NovumCbs\Stam\GeslachtQuery;

$aGeslachten = [
    'Man' => 'M',
    'Vrouw' => 'V',
    'X' => 'X',
];

foreach($aGeslachten as $sLong => $sShort)
{
   $oGeslacht = GeslachtQuery::create()->findOneByNaam($sLong);

   $sLabel = 'updaten';
   if(!$oGeslacht instanceof Geslacht)
   {
       $sLabel = 'toevoegen';
       $oGeslacht = new Geslacht();
   }

   echo "Geslacht " . $sLong . ' ' . $sLabel . PHP_EOL;
   $oGeslacht->setNaam($sLong);
   $oGeslacht->setNaamKort($sShort);
   $oGeslacht->save();

}
