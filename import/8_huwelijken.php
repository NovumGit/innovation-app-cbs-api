<?php
require_once '../../../vendor/autoload.php';
require_once '../../../config/novum.cbs/propel/config.php';
require_once '../../../config/novum.cbs/config.php';

use Model\Custom\NovumCbs\Stam\Huwelijken;
use Model\Custom\NovumCbs\Stam\HuwelijkenQuery;

require_once '../../../vendor/autoload.php';
require_once '../../../config/novum.cbs/propel/config.php';
require_once '../../../config/novum.cbs/config.php';

$aFiles = glob('./voornamen/*');

setlocale(LC_ALL, 'nl_NL');

foreach (range(1971, date('Y')) as $iYear)
{

    $sUrl = 'https://opendata.cbs.nl/ODataApi/odata/37772ned/UntypedDataSet?$filter=((Perioden+eq+%27' . $iYear . 'JJ00%27))&$select=Perioden,+TotaalHuwelijkssluitingen_1';
    $sJson = file_get_contents($sUrl);
    $aJson = json_decode($sJson, true);
    $iPerDag = round($aJson['value'][0]['TotaalHuwelijkssluitingen_1'] / 365);
    foreach (range(1, 12) as $iMonth)
    {
        $monthTs = mktime(0, 0, 1, $iMonth, 1, $iYear);
        $iDaysInMonth = date('t');
        foreach (range(1, $iDaysInMonth) as $iDag)
        {
            $time = mktime(0, 0, 1, $iMonth, $iDag, $iYear);
            echo date('Y-m-d', $time) . PHP_EOL;

            $oHuwelijkenQuery = HuwelijkenQuery::create();
            $oDateTime = new \DateTime();
            $oDateTime->setTimestamp($time);
            $oHuwelijken = $oHuwelijkenQuery->findOneByDatum($oDateTime);

            echo "year $iYear" . PHP_EOL;

            if (!$oHuwelijken instanceof Huwelijken)
            {
                $oHuwelijken = new Huwelijken();
                $oHuwelijken->setDatum($oDateTime);
            }
            else
            {
                echo "update" . PHP_EOL;
                continue;


            }
            $oHuwelijken->setAantal($iPerDag);
            $oHuwelijken->save();
            echo "year $iYear" . PHP_EOL;

            echo "Jaar $iYear, dag $iDag, aantal huwelijken $iPerDag" . PHP_EOL;
        }
    }
    echo "Wait 5 seconds" . PHP_EOL;
    sleep(1);
}
