<?php

use Model\Custom\NovumCbs\Data\Voornaam;
use Model\Custom\NovumCbs\Data\VoornaamQuery;
use Model\Custom\NovumCbs\Stam\GeslachtQuery;

require_once '../../../vendor/autoload.php';
require_once '../../../config/novum.cbs/propel/config.php';
require_once '../../../config/novum.cbs/config.php';

$aFiles = glob('./voornamen/*');


foreach ($aFiles as $sFile)
{
    $sJson = file_get_contents($sFile);

    echo "Loading file $sFile" . PHP_EOL;
    $aNamen = json_decode($sJson);

    if(empty($aNamen))
    {
       continue;
    }
    foreach ($aNamen as $aNaam)
    {
        $sGeslacht = (strpos($sFile, 'jongens')) ? 'M' : 'V';
        $sNaam = $aNaam[0];
        $iAantal = $aNaam[1];
        $iRanking = $aNaam[2];

        $oVoornaam = VoornaamQuery::create()->findOneByNaam($sNaam);

        $sLbl = 'updaten';
        if(!$oVoornaam instanceof Voornaam)
        {
            $sLbl = 'toevoegen';
            $oVoornaam = new Voornaam();
        }
        if(in_array($sNaam, ['Benyamin', 'Abe']))
        {
            continue;
        }

        echo $sNaam . ' ' . $sGeslacht . ' ' . $sLbl . PHP_EOL;
        $oGeslacht = GeslachtQuery::create()->findOneByNaamKort($sGeslacht);
        $oVoornaam->setNaam($sNaam);
        $oVoornaam->setAantal($iAantal);
        $oVoornaam->setPopulariteitPositie($iRanking);
        $oVoornaam->setGeslachtId($oGeslacht->getId());
        $oVoornaam->save();
    }


    /*
echo $sFile . PHP_EOL;

    print_r($aNamen);
    echo $sFile . PHP_EOL;
    */
}
