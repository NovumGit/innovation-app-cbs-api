<?php

use Core\QueryMapper;
use Model\Custom\NovumCbs\Data\InitialPeople;

require_once '../../../vendor/autoload.php';
require_once '../../../config/novum.cbs/propel/config.php';
require_once '../../../config/novum.cbs/config.php';

$sDemograpicsUrl = "https://opendata.cbs.nl/ODataApi/odata/37556/TypedDataSet";
$sDemograpicsData = file_get_contents($sDemograpicsUrl);
$aDemograpicsData = json_decode($sDemograpicsData, true);

QueryMapper::query('DELETE FROM data_initial');

foreach ($aDemograpicsData['value'] as $aRow)
{
    $iJaar = str_replace('JJ00', '', $aRow['Perioden']);

    $i0_20 = $aRow['k_0Tot20Jaar_5'] * 1000;
    $i20_45 = $aRow['k_20Tot45Jaar_6'] * 1000;
    $i45_65 = $aRow['k_45Tot65Jaar_7'] * 1000;
    $i65_80 = $aRow['k_65Tot80Jaar_8'] * 1000;
    $i80_106 = $aRow['k_80JaarOfOuder_9'] * 1000;

    $iMannen = $aRow['Mannen_2'] * 1000;
    $iVrouwen = $aRow['Vrouwen_3'] * 1000;

    $iGehuwd = $aRow['Gehuwd_14'] * 1000;
    $iOngehuwd = $aRow['Ongehuwd_13'] * 1000;
    $iVerweduwd = $aRow['Verweduwd_15'] * 1000;
    $iGescheiden = $aRow['Gescheiden_16'] * 1000;

    $iNoordNL = $aRow['NoordNederland_18'] * 1000;
    $iOostNL = $aRow['OostNederland_19'] * 1000;
    $iWestNL = $aRow['WestNederland_20'] * 1000;
    $iZuidNL = $aRow['ZuidNederland_21'] * 1000;

    echo 'Jaartal ' . $iJaar . PHP_EOL;
    echo 'Gehuwd: ' . $iGehuwd . PHP_EOL;
    echo 'Ongehuwd: ' . $iOngehuwd . PHP_EOL;
    echo 'Verweduwd: ' . $iVerweduwd . PHP_EOL;
    echo 'Gescheiden: ' . $iGescheiden . PHP_EOL;

    echo 'Noord nederland: ' . $iNoordNL . PHP_EOL;
    echo 'Oost nederland: ' . $iOostNL . PHP_EOL;
    echo 'West nederland: ' . $iWestNL . PHP_EOL;
    echo 'Zuid nederland: ' . $iZuidNL . PHP_EOL;
    echo PHP_EOL . PHP_EOL;

    $oInitialPeople = new InitialPeople();

    $oInitialPeople->setJaartal($iJaar);
    $oInitialPeople->setLeeftijd020($i0_20);
    $oInitialPeople->setLeeftijd2045($i20_45);
    $oInitialPeople->setLeeftijd4565($i45_65);
    $oInitialPeople->setLeeftijd6580($i65_80);
    $oInitialPeople->setLeeftijd80106($i80_106);

    $oInitialPeople->setMannen($iMannen);
    $oInitialPeople->setVrouwen($iVrouwen);

    $oInitialPeople->setBurgerlijkeGehuwd($iGehuwd);
    $oInitialPeople->setBurgerlijkeOngehuwd($iOngehuwd);
    $oInitialPeople->setBurgerlijkeGescheiden($iGescheiden);

    $oInitialPeople->setLandsdeelNoord($iNoordNL);
    $oInitialPeople->setLandsdeelOost($iOostNL);
    $oInitialPeople->setLandsdeelWest($iWestNL);
    $oInitialPeople->setLandsdeelZuid($iZuidNL);
    $oInitialPeople->save();
}

