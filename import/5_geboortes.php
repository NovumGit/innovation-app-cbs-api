<?php

use Core\Setting;
use Model\Custom\NovumCbs\Data\Geboorte;
use Model\Custom\NovumCbs\Data\GeboorteQuery;

require_once '../../../vendor/autoload.php';
require_once '../../../config/novum.cbs/propel/config.php';
require_once '../../../config/novum.cbs/config.php';

$aFiles = glob('./voornamen/*');

$sDemograpicsUrl = "https://opendata.cbs.nl/ODataApi/odata/37556/TypedDataSet";
$sDemograpicsData = file_get_contents($sDemograpicsUrl);
$aDemograpicsData = json_decode($sDemograpicsData, true);

$sSettingkey = 'PERSON_BIRTH_IMPORT_YEAR';
/*
echo "Delete all geboortes" . PHP_EOL;
\Core\QueryMapper::query('DELETE FROM data_geboorte');
*/
foreach ($aDemograpicsData['value'] as $aRow)
{
    $iLastImportedYear = Setting::get($sSettingkey);

    $iJaar = str_replace('JJ00', '', $aRow['Perioden']);

    echo 'ly' . $iLastImportedYear . PHP_EOL;

    if($iJaar < $iLastImportedYear)
    {
        echo "Skip $iJaar" . PHP_EOL;
        continue;
    }

    Setting::store($sSettingkey, $iJaar);

    echo "Periode " . $iJaar . PHP_EOL;
    echo 'Levend geborenen ' . ($aRow['Levendgeborenen_62'] * 1000) . PHP_EOL;

    $iJongensGeborenHuidigJaar = ceil(($aRow['Levendgeborenen_62'] * 1000) / ($aRow['LevendgeborenenGeslachtsverhouding_63'] + 1000) * 1000);
    $iMeisjesGeborenHuidigJaar = ceil(($aRow['Levendgeborenen_62'] * 1000) / ($aRow['LevendgeborenenGeslachtsverhouding_63'] + 1000) * $aRow['LevendgeborenenGeslachtsverhouding_63']);

    $iJongensGeborenPerMaand = $iJongensGeborenHuidigJaar / 12;
    $iMeisjesGeborenPerMaand = $iMeisjesGeborenHuidigJaar / 12;

    foreach (range(1, 12) as $iMonthNum)
    {
        $iTimestamp = mktime(0, 0, 0, $iMonthNum, 1, $iJaar);
        $iDaysInThisMonth = date('t');

        $iJongensGeborenPerDagInMaand = ceil($iJongensGeborenPerMaand / $iDaysInThisMonth);
        $iMeisjesGeborenPerDagInMaand = ceil($iMeisjesGeborenPerMaand / $iDaysInThisMonth);

        foreach (range(1, $iDaysInThisMonth) as $iMonthDay)
        {
            $iStatTimestamp = mktime(0, 0, 0, $iMonthNum, $iMonthDay, $iJaar);
            $iDayTimestamp = (new \DateTime())->setTimestamp($iStatTimestamp);

            $oGeboorte = GeboorteQuery::create()->findOneByDatum($iDayTimestamp);

            if($oGeboorte instanceof Geboorte)
            {
                echo "Geboortedag $iMonthDay-$iMonthNum-$iJaar was al gevuld" . PHP_EOL;
                continue;
            }

            if($iJongensGeborenPerDagInMaand == 0)
            {
                continue;
            }
            echo "$iJongensGeborenPerDagInMaand jongens geboren op $iMonthDay-$iMonthNum-$iJaar" . PHP_EOL;
            echo "$iMeisjesGeborenPerDagInMaand meisjes geboren op $iMonthDay-$iMonthNum-$iJaar" . PHP_EOL;

            $oGeboorte = new Geboorte();
            $oGeboorte->setJongensGeboren($iJongensGeborenPerDagInMaand);
            $oGeboorte->setMeisjesGeboren($iMeisjesGeborenPerDagInMaand);
            $oGeboorte->setDatum($iDayTimestamp);
            $oGeboorte->save();
        }
        sleep(0.5);
    }

    echo 'Geboren jongens per jaar: ' . $iJongensGeborenHuidigJaar . PHP_EOL;
    echo 'Geboren meisjes per jaar: ' . $iMeisjesGeborenHuidigJaar . PHP_EOL;
    echo 'Geboren totaal per jaar: ' . ($iJongensGeborenHuidigJaar + $iMeisjesGeborenHuidigJaar) . PHP_EOL . PHP_EOL;
}


