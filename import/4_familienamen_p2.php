<?php

use Model\Custom\NovumCbs\Data\Familienaam;
use Model\Custom\NovumCbs\Data\FamilienaamQuery;

require_once '../../../vendor/autoload.php';
require_once '../../../config/novum.cbs/propel/config.php';
require_once '../../../config/novum.cbs/config.php';

$sXml = file_get_contents('./familienamen/10000-achternamen.xml');
$aXmlFile = json_decode(json_encode(simplexml_load_string($sXml)), true);
$i = 0;
foreach ($aXmlFile['record'] as $aItem)
{
    $i++;
    $oFamilienaamQuery = FamilienaamQuery::create();
    $oFamilienaam = $oFamilienaamQuery->findOneByNaam(trim($aItem['naam']));

    $sLbl = number_format($i, 0,',', '.') . " - Update";
    if(!$oFamilienaam instanceof Familienaam)
    {
        $oFamilienaam = new Familienaam();
        $oFamilienaam->setNaam(trim($aItem['naam']));
        $sLbl = number_format($i, 0,',', '.') . " - Create";
    }
    echo $sLbl . " familienaam " . trim($aItem['naam']) . PHP_EOL;
    $oFamilienaam->setRang($aItem['rang']);
    $oFamilienaam->setPrefix(null);
    if(!empty($aItem['prefix']))
    {
        $oFamilienaam->setPrefix($aItem['prefix']);
    }
    $oFamilienaam->setAantal2007($aItem['n2007']);
    $oFamilienaam->setInTop(1);
    $oFamilienaam->setAantal1947($aItem['n1947']);
    $oFamilienaam->save();


}

exit(__FILE__ . ' all done.' . PHP_EOL);

/*
foreach()
echo $sXmlFile;
*/
