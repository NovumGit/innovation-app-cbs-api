<?php

use Model\Custom\NovumCbs\Data\Familienaam;
use Model\Custom\NovumCbs\Data\FamilienaamQuery;
use Model\Custom\NovumCbs\Data\Voornaam;
use Model\Custom\NovumCbs\Data\VoornaamQuery;
use Model\Custom\NovumCbs\Stam\GeslachtQuery;

require_once '../../../vendor/autoload.php';
require_once '../../../config/novum.cbs/propel/config.php';
require_once '../../../config/novum.cbs/config.php';

$aFiles = glob('./voornamen/*');

foreach(range('a','z') as $char){

    $sUrl = "https://www.cbgfamilienamen.nl/nfb/lijst_namen.php?operator=ew&naam={$char}";
    $sIndexPage = file_get_contents($sUrl);
    $aMatches = [];
    preg_match_all('/\/nfb.+=ew/', $sIndexPage, $aMatches);
    $aNamenUrls = $aMatches[0];

    foreach ($aNamenUrls as $sNaamUrl)
    {
        $sNamenPage = file_get_contents('https://www.cbgfamilienamen.nl' . $sNaamUrl);
        // echo $sNamenPage . PHP_EOL . PHP_EOL;

        preg_match_all('/taal=">(.+)<\/a>/', $sNamenPage, $aNamen);

        foreach ($aNamen[1] as $sAchternaam)
        {
            $oFamilienaam = FamilienaamQuery::create()->findOneByNaam($sAchternaam);

            $sTag = 'updaten';
            if(!$oFamilienaam instanceof Familienaam)
            {
                $oFamilienaam = new Familienaam();
                $sTag = 'toevoegen';
            }
            echo trim($sAchternaam) . " " . $sTag . PHP_EOL;
            $oFamilienaam->setNaam(trim($sAchternaam));
            $oFamilienaam->save();
        }


    }


    // print_r($aMatches);
    // echo "$v \n";
}


foreach ($aFiles as $sFile)
{
    $sJson = file_get_contents($sFile);
    $aNamen = json_decode($sJson);

    foreach ($aNamen as $aNaam)
    {
        $sGeslacht = (strpos($sFile, 'jongens')) ? 'M' : 'V';
        $sNaam = $aNaam[0];
        $iAantal = $aNaam[1];
        $iRanking = $aNaam[2];

        $oVoornaam = VoornaamQuery::create()->findOneByNaam($sNaam);

        $sLbl = 'updaten';
        if(!$oVoornaam instanceof Voornaam)
        {
            $sLbl = 'toevoegen';
            $oVoornaam = new Voornaam();
        }
        if(in_array($sNaam, ['Benyamin', 'Abe']))
        {
            continue;
        }

        echo $sNaam . ' ' . $sGeslacht . ' ' . $sLbl . PHP_EOL;
        $oGeslacht = GeslachtQuery::create()->findOneByNaamKort($sGeslacht);
        $oVoornaam->setNaam($sNaam);
        $oVoornaam->setAantal($iAantal);
        $oVoornaam->setPopulariteitPositie($iRanking);
        $oVoornaam->setGeslachtId($oGeslacht->getId());
        $oVoornaam->save();
    }


    /*
echo $sFile . PHP_EOL;

    print_r($aNamen);
    echo $sFile . PHP_EOL;
    */
}
