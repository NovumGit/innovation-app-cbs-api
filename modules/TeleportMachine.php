<?php
namespace ApiNovumCbs;

use Core\Config;
use Core\Setting;

/**
 * Class Environment
 */
final class TeleportMachine
{
    protected $iCurrentTime = null;

    /**
     * AbstractDemographicJob constructor.
     * @throws \Exception
     */
    final function __construct()
    {
        $this->iCurrentTime = Setting::get('DEMOGRAPHIC_CLOCK');

        if(!$this->iCurrentTime)
        {
            $iTheBeginningOfTime = $this->getTheBeginningOfTime();
            $this->iCurrentTime = $iTheBeginningOfTime;
            Setting::store('DEMOGRAPHIC_CLOCK', $iTheBeginningOfTime);
        }
        echo "The current time is " . $this->getCurrentTime()->format(Config::getDateTimeFormat()) . PHP_EOL;
    }


    final private function getTheBeginningOfTime():int
    {
        return mktime(0, 0, 0, 1, 1, 1900);
    }

    final public function isTheBeginningOfTime():bool
    {
        return ($this->iCurrentTime == $this->getTheBeginningOfTime());
    }

    /**
     * @return int
     * @throws \Exception
     */
    final function getCurrentYear():int
    {
        return $this->getCurrentTime()->format('Y');
    }

    /**
     * @return \DateTime
     * @throws \Exception
     */
    final public function getCurrentTime():\DateTime
    {
        return (new \DateTime())->setTimestamp($this->iCurrentTime);
    }

}
