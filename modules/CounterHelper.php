<?php
namespace ApiNovumCbs;

use Core\Setting;

final class CounterHelper
{
    private $iOriginalQuantity;
    private $sTag;

    function __construct(int $iOriginalQuantity, string $sTag)
    {
        $this->iOriginalQuantity = $iOriginalQuantity;
        $this->sTag = $sTag;
    }


    function needMore():bool
    {
        $iMessagesSend = Setting::get('COUNTER_MESSAGES_SEND_' . $this->sTag, 0);

        if($iMessagesSend < $this->iOriginalQuantity)
        {
            return true;
        }
        return false;
    }
    function increment()
    {
        $iMessagesSend = Setting::get('COUNTER_MESSAGES_SEND_' . $this->sTag, 0);
        Setting::store('COUNTER_MESSAGES_SEND_' . $this->sTag, $iMessagesSend+1);

    }

}
