<?php
namespace ApiNovumCbs;
use Core\Environment;
use GuzzleHttp\Client;

class Sender
{
    static function send(string $sOrigin, string $sTopic, array $aPayload)
    {
        if(Environment::isDevel())
        {
            $sBasUri = 'http://api.burger.demo.novum.nuidev.nl';
        }
        else
        {
            $sBasUri = 'https://api.burger.demo.novum.nu';
        }
        $oClient = new Client(['base_uri' => $sBasUri]);

        $sPayload = json_encode($aPayload);
        $aPostData = [
            'payload' => $sPayload,
            'topic' => $sTopic,
            'origin' => $sOrigin
        ];


        $oResponse = $oClient->request('POST', '/v2/rest/delegatedtasks/', [
            'json' => $aPostData
        ]);

        echo 'body' . $oResponse->getBody() . PHP_EOL;
        echo "Posted " . $sPayload . " got statuscode " . $oResponse->getStatusCode() . PHP_EOL;

    }

}
