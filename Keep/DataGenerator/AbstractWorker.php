<?php
namespace ApiNovumBrp\DataGenerator;

use ApiNovumCbs\DataGenerator\TeleportMachine;

abstract class AbstractWorker
{
    private $oTeleportMachine = null;

    /**
     * AbstractWorker constructor.
     * @throws \Exception
     */
    function __construct()
    {
        $this->oTeleportMachine = new TeleportMachine();
    }

    final public function getTeleportMachine():TeleportMachine
    {
        return $this->oTeleportMachine;
    }
    final public function askGod():GodFactory
    {
        return new GodFactory($this->getTeleportMachine());
    }

    abstract function getTitle() : string ;
    abstract function execute() : void ;
}
