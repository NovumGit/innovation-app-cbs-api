<?php
namespace ApiNovumCbs\DataGenerator;

use ApiNovumBrp\classes\Gods\Priapus;
use ApiNovumBrp\classes\Gods\Thanatos;

final class GodFactory
{

    private $oTeleportMachine;
    function __construct(TeleportMachine $oTeleportMachine)
    {
        $this->oTeleportMachine = $oTeleportMachine;

    }

    function fertilityGod():Priapus
    {
        return new Priapus($this->oTeleportMachine);
    }
    function deathGod():Thanatos
    {
        return new Thanatos($this->oTeleportMachine);
    }
}
