<?php
namespace ApiNovumBrp\data_generator\Calculator;

use ApiNovumBrp\classes\AbstractCalculator;

class Scale extends AbstractCalculator
{
    function calcStep(int $iDifference,int $iTotalSteps,int $iCurrentStep):int
    {
        return ($iDifference / $iTotalSteps) * $iCurrentStep;

    }

}

