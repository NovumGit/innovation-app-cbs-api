<?php
namespace ApiNovumBrp\DataGenerator;


interface IProperty
{
    function get() ;
}
