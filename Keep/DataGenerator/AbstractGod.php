<?php
namespace ApiNovumBrp\classes;


use ApiNovumCbs\DataGenerator\TeleportMachine;

abstract class AbstractGod
{
    private $oTeleportMachine;
    function __construct(TeleportMachine $oTeleportMachine)
    {
        $this->oTeleportMachine = $oTeleportMachine;

    }
    public function getTeleportMachine():TeleportMachine
    {
        return $this->oTeleportMachine;
    }
}
