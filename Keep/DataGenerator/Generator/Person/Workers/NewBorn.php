<?php
namespace ApiNovumCjib\DataGenerator\Generator\Person\Workers;

use ApiNovumBrp\classes\Generator\Person\PropertyFactory;
use ApiNovumBrp\DataGenerator\AbstractWorker;
use ApiNovumBrp\DataGenerator\AllDoneException;
use ApiNovumCbs\DataGenerator\TeleportMachine;
use Model\Custom\NovumBrp\Persoonsgegevens\Persoon;

final class NewBorn extends AbstractWorker
{
    final function getTitle(): string
    {
        return "Making babies";
    }

    /**
     * @throws AllDoneException
     * @throws \Exception
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function execute(): void
    {
        if ($this->getTeleportMachine()->isTheBeginningOfTime()) {

            if(!$this->askGod()->fertilityGod()->shouldWeMakeBabies())
            {
                throw new AllDoneException("The fertility god says we have enough babies now.");
            }

            $oPersoon = new Persoon();

            $oProperties = new PropertyFactory($this->getTeleportMachine(), $oPersoon);

            $oPersoon->setGeslacht($oProperties->getGeslacht()->get());
            $oPersoon->setVoornaam($oProperties->getVoornaam()->get());
            $oPersoon->setGeboortePlaats($oProperties->getGeboorteGemeente()->get());

            $oPersoon->setAchternaam($oProperties->getFamilienaam()->get());
            $oPersoon->setBsn($oProperties->getBSN()->get());
            $oPersoon->setGeboorteDatum($oProperties->getGeboorteDatum()->get());


            $oPersoon->setGeboorteLand($oProperties->getGeboorteLand()->get());
            $oPersoon->save();

            echo 'Baby born: ' . $oPersoon->getVoornaam() . ' ' . trim($oPersoon->getAchternaam()) . ' ' . $oPersoon->getGeslacht()->getNaam() . PHP_EOL;
        }
    }

}
