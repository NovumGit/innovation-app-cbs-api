<?php
namespace ApiNovumBrp\classes\Generator\Person;

use ApiNovumBrp\classes\IProperty;
use ApiNovumBrp\classes\TeleportMachine;
use Model\Custom\NovumBrp\Persoonsgegevens\Persoon;

/**
 * @method Properties\BSN getBSN()
 * @method Properties\Familienaam getFamilienaam()
 * @method Properties\Voornaam getVoornaam()
 * @method Properties\Geslacht getGeslacht()
 * @method Properties\GeboorteDatum getGeboorteDatum()
 * @method Properties\GeboorteGemeente getGeboorteGemeente()
 * @method Properties\GeboorteLand getGeboorteLand()
 */
class PropertyFactory
{

    private $oTeleportMachine;
    private $oPersoon;

    /**
     * PropertyFactory constructor.
     * @param TeleportMachine $oTeleportMachine
     * @param Persoon $oPersoon
     */
    function __construct(TeleportMachine $oTeleportMachine, Persoon $oPersoon)
    {
        $this->oTeleportMachine = $oTeleportMachine;
        $this->oPersoon = $oPersoon;

    }


    /**
     * @param $method
     * @param $args
     */
    public function __call($method, $args):IProperty
    {
        $sClassName = 'ApiNovumBrp\\classes\\Dynamic\\Person\\Properties\\' . str_replace('get', '', $method);
        return new $sClassName();
    }
}
