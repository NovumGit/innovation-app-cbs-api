<?php
namespace ApiNovumCbs\data_generator\Generator\Person\Properties;

use ApiNovumCbs\data_generator\Calculator\Scale;
use ApiNovumCbs\data_generator\IProperty;
use Model\Custom\NovumBrp\Data\FamilienaamQuery;

class Familienaam extends Scale implements IProperty
{
    function __construct()
    {
    }

    function get(): string
    {
        return FamilienaamQuery::create()->getRandom()->getNaam();
    }


}
