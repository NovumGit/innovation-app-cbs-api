<?php
namespace ApiNovumCbs\data_generator\Generator\Person\Properties;

use ApiNovumCbs\data_generator\IProperty;
use Model\Custom\NovumBrp\Stam\GemeenteQuery;

class GeboorteGemeente implements IProperty
{
    /**
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function get():string
    {
        return GemeenteQuery::create()->getRandom()->getNaam();
    }
}
