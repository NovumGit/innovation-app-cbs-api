<?php
namespace ApiNovumCbs\data_generator\Generator\Person\Properties;

use ApiNovumCbs\data_generator\AllDoneException;
use ApiNovumCbs\data_generator\IProperty;
use Model\Custom\NovumBrp\Data\InitialPeople;

class GeboorteDatum implements IProperty
{
    private $iAge = null;

    /**
     * Age constructor.
     * @param InitialPeople $oInitialPeople
     * @throws \Exception
     */
    /*
    function __construct(InitialPeople $oInitialPeople)
    {

        $aPeopleToMake = [
            '0-20' => Setting::get('BOT_VARIATION_AGE_020', $oInitialPeople->getLeeftijd2045()),
            '20-45' => Setting::get('BOT_VARIATION_AGE_2045', $oInitialPeople->getLeeftijd4565()),
            '65-80' => Setting::get('BOT_VARIATION_AGE_6580', $oInitialPeople->getLeeftijd6580()),
            '80-106' => Setting::get('BOT_VARIATION_AGE_80106', $oInitialPeople->getLeeftijd80106())
        ];

        foreach ($aPeopleToMake as $iKey => $iQuantity)
        {
            if($iQuantity <= 0)
            {
                unset($aPeopleToMake[$iKey]);
            }
        }

        if(count($aPeopleToMake) == 0)
        {
            throw new AllDoneException("All ages done");
        }
        $iAgeGroup = array_rand($aPeopleToMake);

        list($iFrom, $iTo) = explode('-', $iAgeGroup);
        $iPeopleToMake = $aPeopleToMake[$iKey] --;

        $sSettingKey = 'BOT_VARIATION_AGE_' . str_replace('-', '', $iAgeGroup);
        Setting::store($sSettingKey, $iPeopleToMake);
        $this->iAge = rand($iFrom, $iTo);

        echo "Creating " . $this->iAge . " year old person" . PHP_EOL;
    }
    */
    function get():\DateTime
    {
        $iAge = rand(0, 100);
        $oGeboorteDatum = (new \DateTime())->setTimestamp(mktime(0, 0, 0, rand(1, 12), rand(0, 31), 1900 - $iAge));
        return $oGeboorteDatum;
    }
}
