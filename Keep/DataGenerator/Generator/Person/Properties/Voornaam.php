<?php
namespace ApiNovumCbs\data_generator\Generator\Person\Properties;

use ApiNovumCbs\data_generator\Calculator\Scale;
use ApiNovumCbs\data_generator\IProperty;
use Model\Custom\NovumBrp\Data\Voornaam as VoornaamModel;
use Model\Custom\NovumBrp\Data\VoornaamQuery;

class Voornaam extends Scale implements IProperty
{
    function __construct()
    {
    }

    /**
     * @return VoornaamModel
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function get(): string
    {
        return VoornaamQuery::create()->getRandom()->getNaam();
    }


}
