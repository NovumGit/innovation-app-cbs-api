<?php
namespace ApiNovumCbs\DataGenerator\Generator\Person\Properties;

use ApiNovumBrp\DataGenerator\IProperty;

class BSN implements IProperty
{

    /**
     * Performs an "11 proef" on the filled in BSN number
     * @param $sBsn
     * @return bool
     */
    private function test($sBsn) {
        $p1 = substr($sBsn,0, 1) * 9;
        $p2 = substr($sBsn,0, 1) * 8;
        $p3 = substr($sBsn,0, 1) * 7;
        $p4 = substr($sBsn,0, 1) * 6;
        $p5 = substr($sBsn,0, 1) * 5;
        $p6 = substr($sBsn,0, 1) * 4;
        $p7 = substr($sBsn,0, 1) * 3;
        $p8 = substr($sBsn,0, 1) * 2;
        $p9 = substr($sBsn,0, 1) * -1;

        $iSum = (int) $p1 + $p2 + $p3 + $p4 + $p5 + $p6 + $p7 + $p8 + $p9;
        $fRemainder = $iSum % 11;
        return ($fRemainder == 0);
    }

    /**
     * Generates a valid BSN number
     * @return int
     */
    function get() {
        while (true) {
            $s = "";
            for ($j = 0; $j < 9; $j++) {
                $y =  floor(rand(0, 9));
                $s = $s . $y;
            }

            if($this->test($s)) {
                return $s;
            }
        }
        return false;
    }

}
