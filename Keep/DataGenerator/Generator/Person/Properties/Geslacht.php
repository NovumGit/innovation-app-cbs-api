<?php
namespace ApiNovumCbs\data_generator\Generator\Person\Properties;

use ApiNovumCbs\data_generator\IProperty;
use Model\Custom\NovumBrp\Data\InitialPeople;
use Model\Custom\NovumBrp\Stam\Geslacht as GeslachtModel;
use Model\Custom\NovumBrp\Stam\GeslachtQuery;

class Geslacht implements IProperty
{

    /**
     * Gender constructor.
     * @param InitialPeople $oInitialPeople
     * @throws \Exception
     */
    function __construct(/*InitialPeople $oInitialPeople*/)
    {
        /*
        $iMenToMake = Setting::get('BOT_VARIATION_MEN', $oInitialPeople->getMannen());
        $iWomanToMake = Setting::get('BOT_VARIATION_WOMAN', $oInitialPeople->getVrouwen());

        $oCurrentGeslacht = null;
        if($iMenToMake > 0 && $iWomanToMake > 0)
        {
            $aRamdomGeslacht = GeslachtQuery::create()->find()->toArray()[rand(0,1)];
            $oCurrentGeslacht = GeslachtQuery::create()->findOneById($aRamdomGeslacht['Id']);
        }
        else if($iMenToMake > 0)
        {
            $oCurrentGeslacht = GeslachtQuery::create()->findOneByNaamKort('M');
        }
        else if($iWomanToMake > 0)
        {
            $oCurrentGeslacht = GeslachtQuery::create()->findOneByNaamKort('V');
        }
        else
        {
            throw new \ApiNovumCbs\data_generator\AllDoneException("All done");
        }
        if(!$oCurrentGeslacht instanceof GeslachtModel)
        {
            return null;
        }

        if($oCurrentGeslacht->getNaamKort() == 'V')
        {
            $iWomanToMake--;
            Setting::store('BOT_VARIATION_WOMAN', $iWomanToMake);
        }
        else if($oCurrentGeslacht->getNaamKort() == 'M')
        {
            $iMenToMake--;
            Setting::store('BOT_VARIATION_MEN', $iMenToMake);
        }

        echo "Mannen nog maken $iMenToMake, Vrouwen nog maken $iWomanToMake" . PHP_EOL;
        $this->oGeslacht = $oCurrentGeslacht;
        */

    }

    function get():?GeslachtModel
    {
        return GeslachtQuery::create()->getRandom();
        // return $this->oGeslacht;
    }
}
