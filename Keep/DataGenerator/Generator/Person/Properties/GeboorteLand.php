<?php
namespace ApiNovumCbs\data_generator\Generator\Person\Properties;

use ApiNovumCbs\data_generator\IProperty;
use Model\Custom\NovumBrp\Stam\LandQuery;

class GeboorteLand implements IProperty
{
    /**
     * @return int
     */
    function get():int
    {
        return LandQuery::create()->findOneByIso2('NL')->getId();
    }
}
