<?php
namespace ApiNovumBrp\classes\Gods;

use ApiNovumBrp\classes\AbstractGod;
use ApiNovumBrp\classes\TeleportMachine;

/**
 * Class Thanatos
 * Thanatos was the Greek god of nonviolent deaths. His name literally translates to “death” in Greek. In some myths, he's considered to be a personified spirit of death rather than a god. The touch of Thanatos was gentle, often compared to the touch of Hypnos, who was the god of sleep
 *
 * @package Crud\Custom\NovumBrp\Persoon\Gods
 */
class Thanatos extends AbstractGod
{

    final public function shouldWeKillSomeone():bool
    {

    }

}
