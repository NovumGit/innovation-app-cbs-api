<?php
namespace ApiNovumBrp\classes\Gods;

use ApiNovumBrp\classes\AbstractGod;
use ApiNovumBrp\classes\Console;
use Model\Custom\NovumBrp\Data\InitialPeopleQuery;
use Model\Custom\NovumBrp\Persoonsgegevens\PersoonQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Class Priapus
 * Priapus was a minor Greek fertility god best known for his large and permanently erect phallus.
 * He was the son of Aphrodite, but there's some question as to whether his father was Pan, Zeus, Hermes,
 * or one of Aphrodite's other numerous lovers.
 *
 * @package Crud\Custom\NovumBrp\Persoon\Gods
 */
final class Priapus extends AbstractGod
{

    /**
     * @return bool
     * @throws \Exception
     */
    final public function shouldWeMakeBabies():bool
    {
        $iCurrentYear = $this->getTeleportMachine()->getCurrentYear();

        echo "Current year $iCurrentYear" . PHP_EOL;
        $oPeopleAlive = InitialPeopleQuery::create()->findOneByJaartal($iCurrentYear);

        $iTotalPeople = $oPeopleAlive->getMannen() + $oPeopleAlive->getVrouwen();

        $oPersoonQuery = PersoonQuery::create();
        $oPersoonQuery->filterBySterfDatum(null, Criteria::ISNULL);
        $iPersonsAlive = $oPersoonQuery->count();

        $iTodo = $iTotalPeople - $iPersonsAlive;
        Console::log("The year is $iCurrentYear, there should be $iTotalPeople alive but we only count $iPersonsAlive. You have $iTodo more todo.");

        return $iPersonsAlive < $iTotalPeople;
    }

}